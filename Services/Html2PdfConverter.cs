﻿namespace html2pdf_react.Services;
using PuppeteerSharp;

public interface IFileConverter
{
    public Task<bool> Html2PdfAsync(string inputFile, string outputFile);
    public Task Init();
}

public class Html2PdfConverter: IFileConverter
{
    private ILogger<Html2PdfConverter> _logger;
    
    public Html2PdfConverter(ILogger<Html2PdfConverter> logger)
    {
        _logger = logger;
    }

    public async Task Init()
    {
        await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultChromiumRevision);
    }
    public async Task<bool> Html2PdfAsync(string inputFile, string outputFile)
    {
        try
        {
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });
            var page = await browser.NewPageAsync();
            await page.GoToAsync((new Uri(inputFile)).AbsoluteUri);
            await page.PdfAsync(outputFile);
            return true;
        }   
        catch (Exception e)
        {
            _logger.LogCritical(e, "Error converting to PDF");
            return false;
        }
    }
}