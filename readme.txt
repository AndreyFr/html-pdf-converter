HTML to PDF converter.

Uses PuppeteerSharp. Created using Asp.Net template.

During launch ensures browser is downloaded.

For scaling and robustness share file system is used. The shared dir is set in appsettings.json as FileStorageDir

If service is restarted during conversion web client will detect error and restarts conversion. If upload is failed client asks for re-upload

After publishing client is under wwwroot directory.

Authorization is not implemented.
