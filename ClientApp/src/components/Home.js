import React, {useRef, useState} from 'react';

export const Home = () => {
    const timeout = 500;
    const attempts = 0; // 0 = retry infinitely
    
    const sleep = t => new Promise(r => setTimeout(r, t));
    
    const [loading, setLoading] = useState(false)
    const [fileReady, setFileReady] = useState(false)
    const [error, setError] = useState('')
    const [success, setSuccess] = useState('')
    const [fileName, setFileName] = useState('')

    const file = useRef()
    
    const uploadError = (fileName) => {
        setError("File upload error. Try again.")
        setLoading(false)
    }
    
    const uploadFile = async (file) => {
        const formData = new FormData()
        formData.append('file', file)
        formData.append('fileName', file.name)
        setLoading(true)
        setError('')
        setSuccess('')
        try {
            let response = await fetch('fileconverter/upload/', {
                method: 'POST',
                body: formData,
            })
            if (response.ok) {
                setSuccess('File uploaded. Converting...')
                setFileName(file.name)
                await checkFile(file.name)
            } else
                uploadError(file.name)
        } catch {
            uploadError(file.name)
        }
    }
    
    const checkFile = async (fileName) => {
        let retryLeft = attempts === 0 ? 1 : attempts
        const nextRetry = r => attempts === 0 ? 1 : r - 1;
        
        while(retryLeft > 0) {
            try {
                let response = await fetch(
                    `fileconverter/checkresult/${fileName}`,
                    {method: 'GET'})
                if( response.ok ){
                    setFileReady(true)
                    setLoading(false)
                    setSuccess("Converted file is ready for download.")
                    retryLeft = 0
                } 
                else if (response.status === 400) {
                    setError("Upload error. Try to upload file again.")
                    setFileReady(false);
                    setLoading(false)
                    retryLeft = 0; 
                }
                else {
                    await sleep(timeout)
                    retryLeft = nextRetry(retryLeft);
                }
            } catch {
                await sleep(timeout)
                retryLeft = nextRetry(retryLeft);
            }
        }
    }
    
    const downloadFile = e => window.open(`fileconverter/downloadfile/${fileName}.pdf`, '_blank')


    const formSubmitted = e => {
        e.preventDefault()
        uploadFile(file.current.files[0])
    }

    const downloadClicked = async e => {
        e.preventDefault()
        await downloadFile(fileName)
    }
    
    return (
        <div>
            <h1>File converter</h1>
            {success === '' || <div className="alert alert-success">{success}</div>}
            {error === '' || <div className="alert alert-danger">{error}</div>}
                {fileReady ? 
                <div><button className="form-control" onClick={downloadClicked}>Download</button></div> 
                :
                    loading ?

                        <div className="d-flex justify-content-center">
                            <div className="spinner-border"></div>
                        </div>
                        :
                        <form onSubmit={formSubmitted}>
                            <label className="form-label" htmlFor="file-selector">Upload file to have it converted from
                                HTML to
                                PDF:</label>
                            <input ref={file} required className="form-control" id="file-selector" type="file"/>
                            <button className="form-control">Convert</button>
                        </form>
            }
        </div>
    );
}
