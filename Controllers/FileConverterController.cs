﻿using html2pdf_react.Services;
using Microsoft.AspNetCore.Mvc;

namespace html2pdf_react.Controllers;

public class UploadForm
{
    public IFormFile file { get; set; }
    public string fileName { get; set; }
}

public class FileConverterController : ControllerBase
{
    private readonly ILogger<FileConverterController> _logger;
    private readonly IConfiguration _configuration;
    private readonly IFileConverter _fileConverter;
    private string SessionDir => Path.Combine(_configuration["FileStorageDir"], HttpContext.Session.Id);

    public FileConverterController(ILogger<FileConverterController> logger, IConfiguration configuration, IFileConverter fileConverter)
    {
        _logger = logger;
        _configuration = configuration;
        _fileConverter = fileConverter;
    }

    
    [HttpPost]
    public async Task<ActionResult> Upload([FromForm] UploadForm form)
    {
        // we need to actually use session for it to be persisted
        HttpContext.Session.SetString("tmp", "1");
        Directory.CreateDirectory(SessionDir);
        string inputFile = Path.Combine(SessionDir, $"{form.fileName}.tmp");
        try
        {
            using (var stream = new FileStream(inputFile, FileMode.Create))
            {
                await form.file.CopyToAsync(stream);
            }

            return StatusCode(StatusCodes.Status201Created);
        }
        catch (Exception e)
        {
            _logger.LogCritical(e, "Error saving file");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpGet]
    public async Task<ActionResult> CheckResult(string id)
    {
        string inputFile = Path.Combine(SessionDir, $"{id}.tmp");
        string outputFile = Path.Combine(SessionDir, $"{id}.pdf");
        
        if (System.IO.File.Exists(outputFile))
            return StatusCode(StatusCodes.Status200OK);
        
        if (!System.IO.File.Exists(inputFile))
            return StatusCode(StatusCodes.Status404NotFound);
        
        var success = await _fileConverter.Html2PdfAsync(inputFile, outputFile);
        return success
            ? StatusCode(StatusCodes.Status200OK)
            : StatusCode(StatusCodes.Status500InternalServerError);
    }

    [HttpGet]
    public ActionResult DownloadFile(string id)
    {
        string outputFile = Path.Combine(SessionDir, $"{id}");
        return PhysicalFile(outputFile, "application/force-download");
    }
}
